export default Vue.component('card', {

    template: `
        <div class="column is-4">
            <div class="card large">
            <div class="card-image is-16by9">
              <figure class="image">
                <img src="https://source.unsplash.com/h-ACUrBngrw/1280x720" alt="Image">
              </figure>
            </div>
            <div class="card-content">
              <div class="media">
                <div class="media-left">
                  <figure class="image is-48x48">
                    <img src="https://avatars.dicebear.com/api/initials/john%20doe.svg" alt="Image">
                  </figure>
                </div>
                <div class="media-content">
                  <p class="is-4 no-padding">Lead Developer</p>
                  <p>
                    <span class="title is-6">
                      <a href="http://twitter.com/twitterid"> twitterid </a>
                    </span>
                  </p> 
                  <p class="subtitle is-6">Lead Developer</p>
                </div>
              </div> 
              <div class="content">
                   would no longer lay eyes upon it, for it had become hideous and twisted.
                <div class="background-icon">
                  <span class="icon-twitter"></span>
                </div>
              </div>
            </div>
          </div>
          </div>
        `,
  
  })